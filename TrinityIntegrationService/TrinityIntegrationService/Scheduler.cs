﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
//using System.Threading.Tasks;
using System.Timers;
using System.Net;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;


namespace TrinityIntegrationService
{
    public partial class Scheduler : ServiceBase
    {
        private Object thisLock = new Object();

        private Timer timer1 = null;

        public Scheduler()
        {
            Program.log.Info("Scheduler()");
            InitializeComponent();
        }

        protected void Actions()
        {
            Program.log.Debug("Enter : Actions()...");

            String strConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            Program.log.Debug("Connection String = " + strConnectionString);

            List<DataResult> outputData = new List<DataResult>();

            SqlConnection conn = new SqlConnection(strConnectionString);
            {
                Program.log.Info("Calling Actions()..." + strConnectionString);

                String strCommand = "";                                                                                                    // AND SourceCode = 'Trinity'
                //strCommand = "SELECT ID, JobNo, RunNo, TrinityJobNo, JobRunStatus, OperationCode, OperationKey, ProgrammerUserName, StartTime, TimeSpent, Filecount, InputRecords, OutputRecords, ExcludedRecords, ParamKey, ParamQuestion, ToUpLoad FROM uv_Timesheet_ToTrinity WHERE JobRun_ID IS NOT NULL AND IsProcessed = 0 AND Chargeable = 1 AND IsCompleted = 1 ORDER BY TrinityJobNo";
                strCommand =  "SELECT	ID, JobNo, RunNo, TrinityJobNo, JobRunStatus, OperationCode, OperationKey, ProgrammerUserName, StartTime, TimeSpent, Filecount, InputRecords, OutputRecords, ExcludedRecords, ParamKey, ParamQuestion, ToUpLoad FROM uv_Trinity_Operations WHERE JobRun_ID IS NOT NULL AND IsProcessed = 0 AND Chargeable = 1 AND IsCompleted = 1 ORDER BY TrinityJobNo ";

                using (SqlCommand cmd = new SqlCommand(strCommand, conn))
                {    
                    try
                    {
                        Program.log.Debug("Opening connection to DB Server..." + conn.State);
                        conn.Open();                   
                    }
                    catch (SqlException e)
                    {
                        Program.log.Fatal(e.Message);
                    }
                
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            DataResult data = new DataResult();
                            data.JobNo = reader["JobNo"].ToString();
                            if (data.JobNo == "")
                                Program.log.Error("JobNo is empty.");                  

                            data.RunNo = reader["RunNo"].ToString();

                            data.TrinityJobNo = reader["TrinityJobNo"].ToString();
                            Program.log.Info("TrinityJobNo = " + data.TrinityJobNo);

                            if (data.TrinityJobNo == "")
                                Program.log.Error("TrinityJobNo is empty.");
                            
                            if (reader["JobRunStatus"].ToString() != "Completed")
                            {
                                Program.log.Info(data.JobNo + " is not completed in IT Schedule and will not be uploaded to Trinity at this time!");
                                continue;
                            }

                            data.SectionKey = "";
                            data.OperationKey = reader["OperationKey"].ToString();
                            Program.log.Info("OperationKey = " + data.OperationKey);

                            data.OperationCode = reader["OperationCode"].ToString();
                            Program.log.Info("OperationCode = " + data.OperationCode);

                            data.ProgrammerUserName = reader["ProgrammerUserName"].ToString();
                            Program.log.Info("ProgrammerUserName = " + data.ProgrammerUserName);

                            data.StartDate = reader["StartTime"].ToString();
                            Program.log.Info("StartTime = " + data.StartDate);
                            
                            data.EndDate = reader["StartTime"].ToString();
                            Program.log.Info("EndTime = " + data.EndDate);

                            data.TimeSpent = reader["TimeSpent"].ToString();
                            Program.log.Info("TimeSpent = " + data.TimeSpent);

                            data.TimeZoneID = "";

                            data.FileCount = reader["Filecount"].ToString();
                            Program.log.Info("Filecount = " + data.FileCount);

                            data.InputRecords = reader["InputRecords"].ToString();
                            Program.log.Info("InputRecords = " + data.InputRecords);

                            data.OutputRecords = reader["OutputRecords"].ToString();
                            Program.log.Info(data.OutputRecords);

                            data.ExcludedRecords = reader["ExcludedRecords"].ToString();
                            Program.log.Info("ExcludedRecords = " + data.ExcludedRecords);

                            data.ParamKey = reader["ParamKey"].ToString();
                            Program.log.Info("ParamKey = " + data.ParamKey);

                            data.ParamQuestion = reader["ParamQuestion"].ToString();
                            Program.log.Info("ParamQuestion = " + data.ParamQuestion);

                            data.ToUpLoad = reader["ToUpLoad"].ToString();
                            Program.log.Info(reader["ToUpLoad"].ToString());

                            outputData.Add(data);
                        }
                    }
                    else
                    {
                        Program.log.Warn("No timesheet record to process...");
                    }
                    
                    Program.log.Info("Reader closed");
                    reader.Close();

                    conn.Close();
                    Program.log.Info("Connection closed..." + conn.State);
                }
            }

            //-- Get Return Code --//

            //Starting...
            //Success:[OK].
            //Response:[<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><RunTimingEventResponse xmlns="http://iq.co.nz/"><RunTimingEventResult xmlns=""><ReturnCode xmlns="http://iq.co.nz/">SUCCESS</ReturnCode><ReturnMessage xmlns="http://iq.co.nz/" /></RunTimingEventResult></RunTimingEventResponse></soap:Body></soap:Envelope>].
            //Done.


            //-- Call Web Service --//

            foreach (DataResult r in outputData)
            {               
                Program.log.Info("Calling Trinity API.." + outputData.Count);          

                int index = outputData.IndexOf(r);
                Program.log.Info("index = " + index);
                DataResult next = null;

                if (index < outputData.Count - 1)
                {
                    next = outputData[index + 1];
                }

                bool isLastRecord = (index == outputData.Count - 1);
               
                String finishParam = "false";
                // Check if it's the last record 
                if (isLastRecord) 
                {
                    finishParam = "true";
                }
                else if (r.TrinityJobNo != next.TrinityJobNo)
                {
                    // It's the last record of the current job
                    finishParam = "true";
                }
                               
                String valToUpload = "";
                Program.log.Info(r.ParamQuestion + " : " + r.ToUpLoad);
                if (r.ToUpLoad == "TimeSpent") {
                    valToUpload = r.TimeSpent;
                } else if (r.ToUpLoad == "ExcludedRecords") {
                    valToUpload = r.ExcludedRecords;
                } else if (r.ToUpLoad == "InputRecords") {
                    valToUpload = r.InputRecords;
                } else if (r.ToUpLoad == "OutputRecords") {
                    valToUpload = r.OutputRecords;
                } else if (r.ToUpLoad == "Filecount") {
                    valToUpload = r.FileCount;
                }
                       
                clsTrinityAPI TrinityAPI = new clsTrinityAPI("");
                Boolean response = TrinityAPI.callAPITime(r.TrinityJobNo, r.SectionKey, r.OperationKey, r.ProgrammerUserName, r.StartDate, "", "", r.TimeZoneID, valToUpload, r.ParamKey, finishParam);

                //WriteToTrinityAPILog(r.TrinityJobNo, r.OperationCode, r.OutputRecords, TrinityAPI.strResponseStatus, TrinityAPI.strResponseXML);
                
                if (response)
                {
                    Program.log.Debug("Success:[" + TrinityAPI.strResponseStatus + "].\n");
                    Program.log.Debug("  Response:[" + TrinityAPI.strResponseXML + "].\n");

                    String connectionStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                    SqlConnection con = new SqlConnection(connectionStr);
                    String timeSheetUpd = null;
                    timeSheetUpd = " UPDATE Timesheet SET IsProcessed = 1 " +
                                   " FROM Timesheet " +
                                   " LEFT JOIN MMWUser ON Timesheet.Programmer_ID = MMWUser.ID " +
                                   " WHERE JobNo = '" + r.JobNo + "' AND RunNo = " + r.RunNo +
                                   " AND MMWUser.UserName='" + r.ProgrammerUserName + "'";
                    using (SqlCommand cmd = new SqlCommand(timeSheetUpd, con))
                    {
                        try
                        {
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        catch (SqlException e1)
                        {
                            Program.log.Fatal("Error occurred while updating Timesheet..." + e1.Message);
                        }
                    }
                }
                else
                {
                    Program.log.Error("Failure: [" + TrinityAPI.strResponseStatus + "].\n");
                    Program.log.Debug("Response:[" + TrinityAPI.strResponseXML + "].\n");
                }
            }

            Program.log.Debug("Exit : Actions()...");
            //CallWebService();

            //-- End --//

            //this.txtStatus.AppendText("Done.\n");
        }

        private void WriteToTrinityAPILog(String trinityJobNo, String opCode, int dpCount, String status, String response)
        {
            String connStr = "Server=VMSYDSQLWEB02;Database=DigitalOps;Trusted_Connection=True";

            SqlConnection con = new SqlConnection(connStr);
            String logTrinityAPI = null;
            logTrinityAPI  = " INSERT INTO TrinityAPILog (TrinityJobNo, OperationCode, DPCount, Status, Response ) ";
            logTrinityAPI += " VALUES ( '";
            logTrinityAPI += trinityJobNo;
            logTrinityAPI += "', ' ";
            logTrinityAPI += opCode;
            logTrinityAPI += "', ' ";
            logTrinityAPI += dpCount;
            logTrinityAPI += "', ' ";
            logTrinityAPI += status;
            logTrinityAPI += "', ' ";
            logTrinityAPI += response;
            logTrinityAPI += "' ) ";

            Program.log.Info(logTrinityAPI);

            using (SqlCommand cmd = new SqlCommand(logTrinityAPI, con))
            {
	            try {
		            con.Open();
		            cmd.ExecuteNonQuery();
                    con.Close();
	            }
	            catch (SqlException e1)
	            {
		            Program.log.Fatal("Error occured while inserting TrinityAPILog... " + e1.Message);
	            }
            }
        }

        protected override void OnStart(string[] args)
        {
            timer1 = new Timer();
            this.timer1.Interval = 30000; //every 30 secs
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            timer1.Enabled = true;

            Program.Start(args);
        }

        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            Program.log.Info("Timer...");
            
            timer1.Enabled = false;

            lock (thisLock)
            {
                Actions();
            }

            timer1.Enabled = true;
            
        }

        protected override void OnStop()
        {
            Program.log.Info("Stop...");
            timer1.Enabled = false;
        }
    }
}
