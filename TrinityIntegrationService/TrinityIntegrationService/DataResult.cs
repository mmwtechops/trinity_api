﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrinityIntegrationService
{
    class DataResult
    {
        private int _ID;
        private String _JobNo;
        private String _RunNo;
        private String _TrinityJobNo;
        private String _OperationCode;
        private String _OperationKey;
        private String _ProgrammerUserName;
        private String _StartDate;
        private String _EndDate;
        private String _TimeSpent;
        private String _TimeZoneID;
        private String _SectionKey;
        private String _ParamKey;
        private String _ParamQuestion;
        private String _FileCount;
        private String _InputRecords;
        private String _OutputRecords;
        private String _ExcludedRecords;

        public String _ToUpLoad;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public String JobNo
        {
            get { return _JobNo; }
            set { _JobNo = value; }
        }

        public String RunNo
        {
            get { return _RunNo; }
            set { _RunNo = value; }
        }

        public String TrinityJobNo
        {
            get { return _TrinityJobNo; }
            set { _TrinityJobNo = value; }
        }

        public String SectionKey
        {
            get { return _SectionKey; }
            set { _SectionKey = value; }
        }


        public String StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }

        public String EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        public String OperationCode
        {
            get { return _OperationCode; }
            set { _EndDate = value; }
        }

        public String OperationKey
        {
            get { return _OperationKey; }
            set { _OperationKey = value; }
        }

        public String ProgrammerUserName
        {
            get { return _ProgrammerUserName; }
            set { _ProgrammerUserName = value; }
        }

        public String TimeSpent
        {
            get { return _TimeSpent; }
            set { _TimeSpent = value; }
        }

        public String TimeZoneID
        {
            get { return _TimeZoneID; }
            set { _TimeZoneID = value; }
        }

        public String ParamKey
        {
            get { return _ParamKey; }
            set { _ParamKey = value; }
        }

        public String ParamQuestion
        {
            get { return _ParamQuestion; }
            set { _ParamQuestion = value; }
        }

        public String FileCount
        {
            get { return _FileCount; }
            set { _FileCount = value; }
        }

        public String InputRecords
        {
            get { return _InputRecords; }
            set { _InputRecords = value; }
        }

        public String OutputRecords
        {
            get { return _OutputRecords; }
            set { _OutputRecords = value; }
        }

        public String ExcludedRecords
        {
            get { return _ExcludedRecords; }
            set { _ExcludedRecords = value; }
        }

        public String ToUpLoad
        {
            get { return _ToUpLoad; }
            set { _ToUpLoad = value; }
        }

    }
}
