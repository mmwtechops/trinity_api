﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Net;
using System.IO;


namespace TrinityIntegrationService
{
    class clsTrinityAPI
    {
        String strAppKey = "25489CC1-BB44-4693-A5B0-1FAA127EFBFC";
        String strAppName = "MMW";

        //private HttpWebRequest webRequest;

        public String strRequestXML;
        public String strResponseXML;
        public String strResponseStatus;

        public String strLogConnectionString;
        //public String strAPITimeURL = "http://trinity.integrations.mmw3degrees.digital/TrinityAPI/values";
        //public String strAPITimeURL = "http://localhost:49720/trinityapi/values";
        //public String strAPITimeURL = "http://localhost:8080";
        public String strAPITimeURL = "http://trinity.mmw3degrees.com.au/PrintIQ/Webservice/ProductionIntegration.asmx";

        //public String strAPITimeAction = "http://trinity.integrations.mmw3degrees.digital/TrinityAPI/values";
        //public String strAPITimeAction = "http://localhost:49720/trinityapi/values";
        //public String strAPITimeAction = "http://trinity.mmw3degrees.com.au/PrintIQ/Webservice/ProductionIntegration.asmx/RunTimingEvent";
        public String strAPITimeAction = "http://iq.co.nz/RunTimingEvent";

        public clsTrinityAPI(String strConnectionString)
        {
            this.strLogConnectionString = strConnectionString;

            this.strRequestXML = "";
            this.strResponseXML = "";
            this.strResponseStatus = "";
        }

        public bool callAPITime(String strJobNo, String strSectionKey, String strOperationKey, String strOperatorUserName, String strStartDate, String strEndDate, String strDuration, String strTimeZoneId, String strParamValue, String strParamKey, String strParamFinish)
        {
            Program.log.Debug("Enter : callAPITime()...");
     
            Program.log.Info("JobNo = " + strJobNo);
            Program.log.Info("SectionKey = " + strSectionKey);
            Program.log.Info("OperationKey = " + strOperationKey);
            Program.log.Info("OperatorUserName = " + strOperatorUserName);
            Program.log.Info("StartDate = " + strStartDate);
            Program.log.Info("EndDate = " + strEndDate);
            Program.log.Info("Duration = " + strDuration);
            Program.log.Info("Param Value = " + strParamValue);
            Program.log.Info("ParameterKey = " + strParamKey);
            Program.log.Info("Param Finish = " + strParamFinish);    

            //-- Create XML Document and Soap Wrapper --//
            
            XmlDocument soapEnvelopeXml = CreateSoapEnvelope(strJobNo, strSectionKey, strOperationKey, strOperatorUserName, strStartDate, strEndDate, strDuration, strTimeZoneId, strParamValue, strParamKey, strParamFinish);
            this.strRequestXML = soapEnvelopeXml.ToString();

            Program.log.Info("strRequestXML = " + this.strRequestXML);
            Program.log.Info("APITimeURL = " + this.strAPITimeURL);
            Program.log.Info("APITimeAction = " + this.strAPITimeAction);

            HttpWebRequest webRequest = CreateWebRequest(this.strAPITimeURL, this.strAPITimeAction);                      

            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            //-- Get The Response From the Web Request --//

            try
            {
                using (WebResponse webResponse = webRequest.GetResponse())
                //using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    //if (webResponse.StatusCode != HttpStatusCode.OK)
                    //    Program.log.Error(webResponse.StatusDescription);

                    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                    {
                        this.strResponseXML = rd.ReadToEnd();               
                    }
                }

                this.strResponseStatus = "OK";
            }
            catch (WebException ex)
            {
                Program.log.Fatal(ex.Message);

                if (ex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)ex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            this.strResponseStatus = reader.ReadToEnd();
                            //TODO: use JSON.net to parse this string and look at the error message
                        }
                    }
                }
            }          

            //XmlTextReader myxml = new XmlTextReader(this.strResponseXML);
            // (myxml.Read())
            //{
            //    if (myxml.NodeType == XmlNodeType.Text)
            //        Program.log.Info(myxml.Value.Trim()); //Console.WriteLine("{0}", reader.Value.Trim());
            //}

            Program.log.Debug("Exit : callAPITime()...");

            //-- Return --//

            if (this.strResponseStatus == "OK")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        
        private HttpWebRequest CreateWebRequest(string url, string action)
        {
            Program.log.Info("Enter : CreateWebRequest() - " + url);
  
            //-- Add WebRequest Headers --//

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);        
            
            /*                      
            HttpWebResponse response = null;
            response = (HttpWebResponse)webRequest.GetResponse();
            if (response == null || response.StatusCode != HttpStatusCode.OK)
            {
                Program.log.Error("WebResponse is either null or not ok..");
            } 
            */

            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            webRequest.KeepAlive = true;  //15 sec on server side
            webRequest.Timeout = 999999999;

            Program.log.Info("Exit : CreateWebRequest()");

            return webRequest;
        }
        

        private XmlDocument CreateSoapEnvelope(String strJobNo, String strSectionKey, String strOperationKey, String strOperatorUserName, String strStartDate, String strEndDate, String strDuration, String strTimeZoneId, String strParamValue, String strParamKey, String strParamFinish)
        {
            Program.log.Info("Enter : CreateSoapEnvelope()..");

            XmlDocument soapEnvelop = new XmlDocument();

            const string SOAPENV_NS = "http://schemas.xmlsoap.org/soap/envelope/";
            //const string WKSP_NS = "http://schemas.ourwebservice.com/message/";

            //-- XML Declaration --//

            XmlDeclaration xmlDeclaration = soapEnvelop.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlElement root = soapEnvelop.DocumentElement;
            soapEnvelop.InsertBefore(xmlDeclaration, root);

            //-- XML Root Element --//

            XmlElement rootEnvelope = soapEnvelop.CreateElement("soap", "Envelope", SOAPENV_NS);
            rootEnvelope.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            rootEnvelope.SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            //rootEnvelope.SetAttribute("xmlns:wksp", WKSP_NS);
            rootEnvelope.SetAttribute("xmlns:soap", SOAPENV_NS);
            soapEnvelop.AppendChild(rootEnvelope);

            // Create soapenv:Body 

            XmlElement bodyNode = soapEnvelop.CreateElement("soap", "Body", SOAPENV_NS);

            // Create RunTimingEvent

            XmlElement timingEventNode = soapEnvelop.CreateElement("RunTimingEvent");
            timingEventNode.SetAttribute("xmlns", "http://iq.co.nz/");

            // Add RunTimingEvent XML Elements --//

            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "AppKey", this.strAppKey));
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "AppName", this.strAppName));
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "JobNo", strJobNo));
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "SectionKey", strSectionKey));

            if (strOperationKey == "")
            {
                Program.log.Warn("OperationKey is empty.");
            }
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "OperationKey", strOperationKey));          
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "OperatorUserName", strOperatorUserName));

            if (strStartDate == "")
            {
                Program.log.Warn("StartDate is empty.");
            }
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "StartDate", strStartDate));
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "StartDate", strEndDate));
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "Duration", strDuration));             
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "TimeZoneId", strTimeZoneId));

            if (strParamKey == "")
            {
                Program.log.Error("Parameter Key is empty.");
            }
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "ParameterKey", strParamKey));

            if (strParamValue == "")
            {
                Program.log.Error("Parameter Value is empty.");
            }
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "ParameterValue", strParamValue));
            
            //Upload will only happen when job status is set to Completed; so, Finish param will always be true.
            //timingEventNode.AppendChild(addXMLElement(soapEnvelop, "Finish", "true"));
            timingEventNode.AppendChild(addXMLElement(soapEnvelop, "Finish", strParamFinish));

            //-- Finish Body --//

            bodyNode.AppendChild(timingEventNode);
            rootEnvelope.AppendChild(bodyNode);

            Program.log.Info("Exit : CreateSoapEnvelope()..");

            return soapEnvelop;
        }

        private XmlElement addXMLElement(XmlDocument xmlDocument, String strElementName, String strElementText)
        {
            Program.log.Debug("Enter : AddXMLElement().." + strElementName);

            XmlElement xmlElement = xmlDocument.CreateElement(strElementName);
            xmlElement.SetAttribute("xmlns", "");
            xmlElement.AppendChild(xmlDocument.CreateTextNode(strElementText));

            Program.log.Debug("Exit : AddXMLElement()..");

            return xmlElement;
        }

        private void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            Program.log.Info("Enter : InsertSoapEnvelopeIntoWebRequest()");
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
            Program.log.Info("Exit : InsertSoapEnvelopeIntoWebRequest()");
        }
    }
}
