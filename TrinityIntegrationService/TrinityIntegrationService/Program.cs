﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using log4net;
using log4net.Config;


namespace TrinityIntegrationService
{
    static class Program
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new Scheduler() 
            };
            ServiceBase.Run(ServicesToRun);

            XmlConfigurator.Configure();
        }
     
        public static void Start(string[] args)
        {
            log.Info("Start..");        
        }

        public static void Stop()
        {
            log.Info("Stop..");
        }       
    }
}
